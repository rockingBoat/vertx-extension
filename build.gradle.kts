import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.3.10"
    `maven`
}

group = "rockingBoat"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    compile(kotlin("stdlib-jdk8"))
    compile("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.0.1")
    compile("io.vertx:vertx-core:3.6.0.CR1")
    compile("io.vertx:vertx-lang-kotlin-coroutines:3.6.0.CR1")
    compile("io.vertx:vertx-web:3.6.0.CR1")
    compile("io.vertx:vertx-web-client:3.6.0.CR1")
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}