package rockingBoat.vertxExtension

import io.vertx.core.MultiMap
import io.vertx.core.buffer.Buffer
import io.vertx.ext.web.client.HttpRequest
import kotlin.coroutines.resume
import kotlin.coroutines.resumeWithException
import kotlin.coroutines.suspendCoroutine


data class ServiceResponse(
    val statusCode: Int,
    val statusMessage: String,
    val headers: MultiMap,
    val body: Buffer?
)

@SuppressWarnings("unused")
suspend fun HttpRequest<Buffer>.sendBuffer(buffer: Buffer) = suspendCoroutine<ServiceResponse> { cont ->
    sendBuffer(buffer) { ar ->
        ar.result()?.let {
            cont.resume(
                ServiceResponse(
                    statusCode = it.statusCode(),
                    statusMessage = it.statusMessage(),
                    headers = it.headers(),
                    body = it.bodyAsBuffer()
                )
            )
        } ?: cont.resumeWithException(ar.cause())

    }
}

@SuppressWarnings("unused")
suspend fun HttpRequest<Buffer>.send() = suspendCoroutine<ServiceResponse> { cont ->
    send { ar ->
        ar.result()?.let {
            cont.resume(
                ServiceResponse(
                    statusCode = it.statusCode(),
                    statusMessage = it.statusMessage(),
                    headers = it.headers(),
                    body = it.bodyAsBuffer()
                )
            )
        } ?: cont.resumeWithException(ar.cause())
    }
}
